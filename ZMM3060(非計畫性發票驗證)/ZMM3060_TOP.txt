*&---------------------------------------------------------------------*
*&  INCLUDE           ZMM3060_TOP
*&---------------------------------------------------------------------*
DATA: ls_headerdata LIKE bapi_incinv_create_header,
              lt_itemdata         LIKE TABLE OF bapi_incinv_create_item,
              wa_itemdata   LIKE bapi_incinv_create_item,
              ls_itemdata         LIKE bapi_incinv_create_item OCCURS 0 WITH HEADER LINE,
              wa_taxdata      LIKE bapi_incinv_create_tax,
              lt_taxdata      LIKE TABLE OF bapi_incinv_create_tax,
              lt_return           LIKE bapiret2 OCCURS 0 WITH HEADER LINE,
              ls_return           LIKE bapiret2,
             lc_belnr LIKE bapi_incinv_fld-inv_doc_no,
             lc_gjahr LIKE bapi_incinv_fld-fisc_year,
             d_msg(110),
             i_return LIKE bapiret2 OCCURS 0 WITH HEADER LINE,
             lv_invoicedocnumber LIKE bapi_incinv_fld-inv_doc_no,
             lv_fiscalyear       LIKE bapi_incinv_fld-fisc_year.
DATA:temp_budat LIKE sy-datum.
DATA: ok_code TYPE sy-ucomm,
      save_ok LIKE sy-ucomm.

*** 控制變數
DATA: gi_fkey TYPE TABLE OF sy-ucomm.
*DATA: gw_runtime(1)      VALUE c_initial_mode, " Runtime Mode
*              gw_newversion(1)   VALUE space,          " 新版
*              gw_tcode_call(1)   VALUE space,          " 其他程式呼叫
*              gw_input_datar(1)  VALUE space,          " 資料有無變更
*              gw_anser_code(1)   VALUE space,          " 離開詢問變數
*              gw_tmpreturn_ok(1) VALUE space.          " 暫存 Return 值
*
*DATA:BEGIN OF it_temp OCCURS 0,
*            XBLNR LIKE zmm3060_str-XBLNR, "文件號碼
*            LIFNR LIKE zmm3060_str-LIFNR, "開票方
*            BLDAT LIKE zmm3060_str-BLDAT, "文件日期
*            BUDAT LIKE zmm3060_str-BUDAT, "過帳日期
*            ZFBDT LIKE zmm3060_str-ZFBDT,   "基礎日期
*            RMWWR LIKE zmm3060_str-RMWWR, "總金額
*            MWSKZ1 LIKE zmm3060_str-MWSKZ1, "稅碼
*            WAERS LIKE zmm3060_str-WAERS, "幣別
*            BEZNK LIKE zmm3060_str-BEZNK, "未計劃交貨成本
*            FWBAS LIKE zmm3060_str-RMWWR, "基礎金額
*            WMWST1 LIKE zmm3060_str-WMWST1, "稅額
*            TEXT1 LIKE zmm3060_str-TEXT1,  "付款條件
*            KIDNO LIKE zmm3060_str-KIDNO, "付款參考
*            SGTXT LIKE zmm3060_str-SGTXT, "付款條件說明
**  INCLUDE STRUCTURE zmm3060_str.
*       END OF it_temp.
 TABLES: zmm3060_str .
 DATA it_temp LIKE zmm3060_str OCCURS 5 WITH HEADER LINE.


*** BDC变量
DATA: it_bdcdata  TYPE TABLE OF bdcdata,
      wa_bdcdata  TYPE          bdcdata,
      it_bdcmsg   TYPE TABLE OF bdcmsgcoll,
      wa_bdcmsg   LIKE  LINE OF it_bdcmsg.
